#
#  Be sure to run `pod spec lint RXChat.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "RXChat"
  spec.version      = "2.0.0"
  spec.summary      = "容信IM插件"

  spec.description  = <<-DESC
                  容信插件RXChat模块，依赖RXAppModel,RXCommon
                   DESC

  spec.homepage     = "https://www.yuntongxun.com/"
  spec.license      = "MIT"
  spec.author             = { "gaoyuan" => "2502905737@qq.com" }
  spec.ios.deployment_target = "9.0"
  spec.source          = { :git => "https://gitlab.com/RXPlugins/RXChat.git", :tag => "#{spec.version}" }
  spec.resources       = ["source/*.bundle","source/ChatXIB/*.xib","source/消息/*.png","source/聊天/**/*.png","source/加号弹框/*.png"]

  # spec.subspec 'source' do |ss|
    
  #   ss.subspec '公共' do |sss|
  #   sss.source_files = "source/公共/*.png"
  #   end

  #   ss.subspec '加号弹框' do |sss|
  #   sss.source_files = "source/加号弹框/*.png"
  #   end
 
  #   ss.subspec '聊天' do |sss|
  #   sss.source_files = "source/聊天/**/*.png"
  #   end

  #   ss.subspec '消息' do |sss|
  #   sss.source_files = "source/消息/*.png"
  #   end

  # ss.subspec 'ChatXIB' do |sss|
  # sss.source_files = "source/ChatXIB/*.xib"
  # end

  # end

  spec.framework    = "PushKit"
  spec.source_files    = "header/*.h"
  spec.vendored_library = 'lib/*.a'
  spec.vendored_frameworks = 'lib/*.framework'
  spec.requires_arc = true
  spec.dependency "RXCommon"
  spec.dependency "RXAppModel"

end
